package com.huy.zika;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.huy.zika.work.ChartDraw;
import com.huy.zika.work.GetDB;
import com.huy.zika.work.GetFile;
import com.huy.zika.work.PushDB;

@SpringBootApplication
public class ZikaApplication {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		SpringApplication.run(ZikaApplication.class, args);
		GetFile g = new GetFile();
		TreeMap<String, TreeMap<String, List<Integer>>> rData = g.readCSV();
		
		Runnable myRunnable2 = new Runnable() {
			public void run() {
				synchronized (rData) {
					for (Map.Entry<String, TreeMap<String, List<Integer>>> entryg : rData.entrySet()) {
					
						PushDB put = new PushDB(entryg);
						put.run();
					}
				}
			}
		};
		Thread thread1 = new Thread(myRunnable2);
		thread1.start();

		for (Map.Entry<String, TreeMap<String, List<Integer>>> entryp : rData.entrySet()) {
			
			Runnable myRunnable = new Runnable() {
				public void run() {
					synchronized (rData) {
						GetDB get = new GetDB(entryp);
						TreeMap<String, List<Integer>> data = get.run();
						ChartDraw draw = new ChartDraw(entryp.getKey(), data);
						draw.draw();
					}
				}
			};
			Thread thread = new Thread(myRunnable);
			thread.start();

		}

	}

}
