package com.huy.zika.work;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Stream;

public class GetFile {

	public TreeMap<String,TreeMap<String,List<Integer>>> readCSV() throws FileNotFoundException, IOException {

		TreeMap<String,TreeMap<String,List<Integer>>> map = new TreeMap<String, TreeMap<String,List<Integer>>>();
		try (Stream<Path> paths = Files.walk(Paths.get("ZikaData"))) {
			paths.filter(Files::isRegularFile).forEach((e) -> {
				String s = e.toString();
				String date = s.substring(s.indexOf("2"), s.indexOf("."));

				try (BufferedReader br = new BufferedReader(new FileReader(e.toString()))) {
					String line;
					line = br.readLine();
				
					
					while ((line = br.readLine()) != null) {
						TreeMap<String,List<Integer>> lls = new TreeMap<String,List<Integer>>();
						String[] values = line.split(",");
						
						List<Integer> ls = new ArrayList<>();
						String newval = values[0].replace(" ", "_");
						newval = newval.replace("†", "");
						newval = newval.replace("‡", "");
						newval = newval.replace(".", "");
						
						if(!map.containsKey(newval)) {
							ls.add(Integer.parseInt(values[1]));
							ls.add(Integer.parseInt(values[1]));
							lls.put(date,ls);
							map.put(newval, lls);
						}else {
							Entry<String,List<Integer>> ent = map.get(newval).lastEntry();
							ls.add(Integer.parseInt(values[1]));
							ls.add(ent.getValue().get(1)+ls.get(0)+Integer.parseInt(values[2]));
							map.get(newval).put(date,ls);
						}
						
						
					}
					
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});
		}
		return map;
	}
}