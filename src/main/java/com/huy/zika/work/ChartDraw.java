package com.huy.zika.work;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

public class ChartDraw {
	private static TreeMap<String,List<Integer>> data;
	private static String title;
	public ChartDraw(String title,TreeMap<String,List<Integer>> data) {
		this.title=title;
		this.data=data;
	
	}
	
	public void draw()
	 {
	
		 XYDataset xydataset = createPriceDataset();
		 
		  JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(title, "Date", "Affected Case", xydataset, true, true, false);
		  XYPlot xyplot = (XYPlot)jfreechart.getPlot();
		  NumberAxis numberaxis = (NumberAxis)xyplot.getRangeAxis();
		  numberaxis.setLowerMargin(0.40000000000000002D);
		
		  XYItemRenderer xyitemrenderer = xyplot.getRenderer();
		  xyitemrenderer.setDefaultToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")));
		  NumberAxis numberaxis1 = new NumberAxis("Total");
		  numberaxis1.setUpperMargin(1.0D);
		  xyplot.setRangeAxis(1, numberaxis1);
		  xyplot.setDataset(1, createVolumeDataset());
		  xyplot.setRangeAxis(1, numberaxis1);
		  xyplot.mapDatasetToRangeAxis(1, 1);
		  XYBarRenderer xybarrenderer = new XYBarRenderer(0.20000000000000001D);
		  xybarrenderer.setDefaultToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0,000.00")));
		  xyplot.setRenderer(1, xybarrenderer);
	  try {
		ChartUtils.saveChartAsPNG(new File("picture/"+title+".png"), jfreechart, 700, 400);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  System.out.println("Generated PNG file:"+title);
	 }

	 private static XYDataset createPriceDataset()
	 {
	  TimeSeries timeseries = new TimeSeries("Total");
	  for (Map.Entry<String,List<Integer>> entry : data.entrySet()) {
		  int[] day = Stream.of(entry.getKey().split("-")).mapToInt(Integer::parseInt).toArray();
		  timeseries.add(new Day(day[2], day[1], day[0]), entry.getValue().get(0));
	}
	 
	  return new TimeSeriesCollection(timeseries);
	 }

	 private static IntervalXYDataset createVolumeDataset()
	 {
	  TimeSeries timeseries = new TimeSeries("Affected Case");
	  for (Map.Entry<String,List<Integer>> entry : data.entrySet()) {
		  int[] day = Stream.of(entry.getKey().split("-")).mapToInt(Integer::parseInt).toArray();
		  timeseries.add(new Day(day[2], day[1], day[0]), entry.getValue().get(1));
	}
	
	  return new TimeSeriesCollection(timeseries);
	 }

}

