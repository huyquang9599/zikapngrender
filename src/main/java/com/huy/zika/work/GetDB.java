package com.huy.zika.work;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class GetDB {

	static final String JDBC_DRIVER = "org.h2.Driver";
	static final String DB_URL = "jdbc:h2:file:./data/demo";

	// Database credentials
	static final String USER = "sa";
	static final String PASS = "123";

	private Entry<String, TreeMap<String, List<Integer>>> entry;
	private TreeMap<String, List<Integer>> data= new TreeMap<String, List<Integer>>();

	public GetDB(Map.Entry<String, TreeMap<String, List<Integer>>> entry) {
		this.entry = entry;

	}

	public TreeMap<String, List<Integer>> run() {
		Statement stmt = null;
		Connection conn = null;

		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String newval = entry.getKey();
			String sql = "Select * from " + newval;
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				List<Integer> ls = new ArrayList<Integer>();
				ls.add(rs.getInt(3));
				ls.add(rs.getInt(4));
				 data.put(rs.getDate(2).toString(),ls );
				//System.out.println(String.format("%s:(%d/%d)", rs.getDate(2).toString(), rs.getInt(3), rs.getInt(4)));

			}
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return data;
	}

}
