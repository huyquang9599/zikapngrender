package com.huy.zika.work;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class PushDB {

	static final String JDBC_DRIVER = "org.h2.Driver";
	static final String DB_URL = "jdbc:h2:file:./data/demo";

	// Database credentials
	static final String USER = "sa";
	static final String PASS = "123";

	private Entry<String, TreeMap<String, List<Integer>>> entry;

	public PushDB(Map.Entry<String, TreeMap<String, List<Integer>>> entry) {
		this.entry = entry;
	}

	public void run() {
		Statement stmt = null;
		Connection conn = null;

		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();

			for (Map.Entry<String, List<Integer>> val : entry.getValue().entrySet()) {
				String newval = entry.getKey();
				String sql = "CREATE TABLE IF NOT EXISTS " + newval + "(id INTEGER not NULL auto_increment, "
						+ " Date Date, " + " affectedCase INTEGER, " + " total INTEGER, " + " PRIMARY KEY ( id ))";
				stmt.executeUpdate(sql);
				
					String sqladd = String.format(
							"INSERT INTO %s (date,affectedCase,total)"
									+ "VALUES (PARSEDATETIME('%s', 'yyyy-MM-dd'),%s,%d);",
							newval, val.getKey(), val.getValue().get(0), val.getValue().get(1));
					 stmt.executeUpdate(sqladd);
				
				}
			

			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

	}
}
